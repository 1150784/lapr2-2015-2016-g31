﻿# UC16 - Taxa de aceitação #

O Gestor/Organizador de Exposições inicia a taxa de aceitação. O sistema lista as Exposições existentes. O gestor/Organizador escolhe. O sistema mostra a taxa de aceitação de expositores e os valores médios de cada critério de submissão. O caso de uso termina.

## SSD - Formato Breve ##

![uc16.jpg](https://bitbucket.org/repo/8aRMng/images/2567339092-uc16.jpg)

## Ator primário ##
* Gestor de Exposições/Organizador de Exposições.

## Partes interessadas e seus interesses ##

* Gestor/Organizador de Exposições : Tem assim uma forma de saber a estatística da taxa de aceitação de expositores por cada exposição, e também os valores médios de cada critério de submissão.


## Pré-condições ##


## Pós-condições ##


## Cenário de sucesso principal (ou fluxo básico) ##
1. O Gestor/Organizador de Exposições inicia taxa de aceitação.
2. O sistema lista todas as exposições e pede para escolher uma.
3. O Gestor/Organizador escolhe uma exposição.
4. O sistema mostra a taxa de aceitação de expositores e os valores médios de cada critério de submissão.
5. O Caso de uso termina.

## Extensões (ou fluxos alternativos) ##
* *a. O Gestor de Exposições solicita o cancelamento da operação. 
> 1. O caso de uso termina.

* 2.a Não há candidaturas na exposição 
>1. O sistema informa o utilizador e o caso de uso termina.


## Requisitos especiais ##
* Não se verificam.

## Lista de variações em tecnologias e dados ##
* Não se verificam.

## Frequência de Ocorrência ##
* Desconhecida.

## Questões em aberto ##
* Este caso de uso irá estar implementado dentro de cada exposição ou o Gestor/Organizador inicia-o e depois escolhe qual exposição pretende ver?  

