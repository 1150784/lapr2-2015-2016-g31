# UC23 Confirmar Stand
##	Formato breve
O representante do expositor inicia no sistema o processo de confirmar o stand atribuído a uma candidatura . O sistema mostra  a lista de candidaturas a que "ligado" , com a informação das mesmas, solicitando a escolha de uma. O representante seleciona uma candidatura e o sistema mostra informação do stand a que a candidatura está atribuída e pede a escolha de uma decisão ( SIM/NÃO , se pretende ou não atribuir o stand á candidatura). O representante seleciona uma opção e o sistema pede confirmação. O representante do expositor confirma e o sistema informa o sucesso da operação.
##	SSD de formato breve

![Sequence Diagram1.jpg](https://bitbucket.org/repo/8aRMng/images/1488161028-Sequence%20Diagram1.jpg)


##	Formato completo

###Ator principal
* Representante do Expositor
###Partes interessadas e seus interesses
* Representante do Expositor : pretender confirmar o stand atribuído á candidatura.

###Pré-condições###
* Candidaturas que estejam no estado registada.
* Candidaturas que tenham stands atribúidos.
* Representante do expositor com candidaturas registadas.

###Pós-condições###
* Candidaturas selecionadas no estado retirada.

###Cenário de sucesso principal###
1. O representante do expositor inicia no sistema o processo de confirmar o stand atribuído a uma candidatura
2.  O sistema mostra a lista de candidaturas a que o representante está ligado e solicita a escolha de uma.
3.  O representante seleciona uma candidatura.
4. O sistema mostra o stand atribuído á candidatura e sua descrição e pede a escolha de um decisão (se pretende ou não atribuir o stand á candidatura)
5. O representante seleciona uma opção.
6. O sistema pede confirmação.
7. O representante confirma.
8. Os passos 2 a 8 repetem-se enquanto o representante pretender retirar mais candidaturas.
8. O sistema informa o sucesso da operação.



### Extensões ###
*a. O representante  solicita cancelamento da operação confirmar Stands.

> O caso de uso termina.

5b.  O representante seleciona a opção "NÃO"

1.  O caso de uso termina volta ao passo 2.

7c.  O representante não confirma a atribuição.

1.  O caso de uso volta ao passo 2.


##Requisitos especiais
*

##Lista de variações em tecnologias e dados
* 

##Frequência de Ocorrência
* 

##Questões em aberto
1.	Qual a frequência de ocorrência deste caso de uso?
