﻿# UC21 - Atribuir Stands #

 O Organizador de Exposições inicia o processo de atribuibuição de stands.  O sistema apresenta uma lista das candidaturas com decisão. O Organizador seleciona uma. O sistema apresenta a informação da candidatura, apresenta uma lista dos stands e solicita a escolha de um. O Organizador seleciona o stand. O sistema pergunta ser quer continuar. O Organizador responde afirmativamente. O sistema pede confirmação. O Organizador confirma. O sistema regista a atribuição e termina o processo.

## SSD - Formato Breve ##
![SSD_UC21.png](UC21.jpg)

## Ator primário ##
* Organizador de Exposições.

## Partes interessadas e seus interesses ##
* Centro de Exposição: Ter os stands atribuidos às candidaturas
* Organizador de Exposições : Ter uma forma de atribuir os stands de forma rapida e eficiente


## Pré-condições ##
* Os Stands estão criados
## Pós-condições ##
* Os Stands estão devidamente atribuidos.

## Cenário de sucesso principal (ou fluxo básico) ##
1. O Organizador de Exposições inicia o processo de atribuibuição de stands. 
2. O sistema apresenta uma lista das candidaturas com decisão.
3. O Organizador seleciona uma.
4. O sistema apresenta a informação da candidatura, apresenta uma lista dos stands e solicita a escolha de um.
5. O Organizador seleciona o stand.
6. O sistema pergunta ser quer continuar.
7. O Organizador responde afirmativamente.
8. O sistema pede confirmação.
9. O Organizador confirma.
10. O sistema regista a atribuição e termina o processo.

## Extensões (ou fluxos alternativos) ##
* *a. O Organizador de Exposições solicita o cancelamento da operação. 
> 1. O caso de uso termina.
* 2a. Não ha candidaturas com decisão
> 1. O sistema informa da falta de condições e encerra o caso de uso.

## Requisitos especiais ##
* Não se verificam.

## Lista de variações em tecnologias e dados ##
* Não se verificam.

## Frequência de Ocorrência ##
* Desconhecida.

## Questões em aberto ##
* 

