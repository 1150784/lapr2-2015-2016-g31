﻿
##	UC3 - Atribuir candidatura

##	Formato breve
O organizador inicia o processo de atribuição de candidaturas. O sistema disponibiliza uma lista de exposições(ainda em periodo de atribuição) e pede para que uma delas seja selecionada. O organizador introduz a exposição. O sistema valida e apresenta uma lista de candidaturas à exposição, pedindo que seja escolhido um mecanismo de atribuição de candidaturas. O organizador especifica o mecanismo de atribuição. O sistema apresenta as associações resultantes desse mecanismo ao organizador pedindo que os confirme. O organizador confirma, o sistema regista as associações e informa o sucesso da operação.

##	SSD de formato breve
![SSD UC3.jpg](UC3.jpg)

##	Formato completo

###Ator principal
Organizador

###Partes interessadas e seus interesses
+ Organizador: Pretende associar candidaturas dos expositores aos funcionários de apoio à exposição de forma rápida e automatica.
+ Centro de exposições: Pretende ter exposições validadas por entidades selecionadas atraves dos mecanismos escolhidos pelo organizador.
+ FAE: necessitam saber que candidatura vao avaliar.

###Pré-condições
+ Utilizador autenticado no sistema como Organizador.

###Pós-condições
+ A tabela de associações de candidaturas a FAE é atualizada.

###Cenário de sucesso principal (ou fluxo básico)
1. O organizador inicia no sistema a atribuição de candidaturas.
2. O sistema mostra uma lista de exposições ativas e solicita escolha de uma.
3. O organizador seleciona uma exposição.
4. O sistema valida a exposição, mostra uma lista de candidaturas à exposição e solicita a escolha de um mecanismo de atribuição.
5. O organizador seleciona um mecanismo.
6. O sistema valida o mecanismo, mostra as associaçoes resultantes do mesmo e pede confirmação.
7. Os passos 5-8 repetem-se até que o organizador aceite as associações resultantes, ou  decida terminar.
8. O organizador confirma.
9. O sistema regista a atribuição e informa o sucesso da operação.

###Extensões (ou fluxos alternativos)
*a. A qualquer momento o organizador cancela o processo.

+ O caso de uso termina.

2a. Não há exposições nas condições especificadas.

+ O sistema avisa o organizador e o caso de uso termina.

3a. A exposição introduzida pelo organizador não está nas condições necessárias.

+ O sistema alerta para o facto. O sistema permite a alteração da identificação introduzida.

4a. Não há candidaturas à exposição.

+ O sistema avisa o organizador e o caso de uso termina.

5a. O mecanismo introduzido não corresponde a nenhum mecanismo do sistema.

+ O sistema alerta para o facto. O sistema permite a alteração do mecanismo introduzido.


##Requisitos especiais
- 

##Lista de variações em tecnologias e dados
-

##Frequência de Ocorrência
-

##Questões em aberto


