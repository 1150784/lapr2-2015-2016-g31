# UC19 Retirar Candidatura
##	Formato breve
O representante do Expositor inicia no sistema o processo de retirar candidaturas. O sistema lista todas as candidaturas que ainda estão no estado registada. O representante seleciona  uma exposição que pretende retirar candidatura. O sistema mostra as informações relativas à candidatura e pede confirmação. O sistema solicita ao representante se pretende retirar mais candidaturas e o representante seleciona opção (este processo de selecionar candidaturas repete-se até quando o representante pretender continuar).  Posteriormente o sistema informa o sucesso da operação.

##	SSD de formato breve

![Sequence Diagram1.jpg](https://bitbucket.org/repo/8aRMng/images/1034996642-Sequence%20Diagram1.jpg)


##	Formato completo

###Ator principal
* Representante de Expositor
###Partes interessadas e seus interesses
* Representante do Expositor : pretender retirar as candidaturas

###Pré-condições###
* Candidaturas que estejam no estado registada.

###Pós-condições###
* Candidaturas selecionadas no estado retirada.

###Cenário de sucesso principal###
1. O representante do expositor inicia no sistema o processo de retirar candidaturas.
2. O sistema lista todas as candidaturas que estejam no estado registada.
3. O representante do expositor seleciona uma exposição.
4. O sistema mostra as informações relativas à candidatura e pede confirmação.
5. O representante confirma .
6. O sistema solicita se pretende continuar ou retirar mais candidaturas.
7. O representante seleciona a opção.
8. Os passos 2 a 7 repetem-se enquanto o representante pretender retirar mais candidaturas.
9. O sistema informa o sucesso da operação.


### Extensões ###
*a. O representante  solicita cancelamento da operação de retirar candidaturas.

> O caso de uso termina.

5a.  O representante não seleciona a opção "confirmar"

1. O representante volta a escolher uma candidatura que pretende selecionar.

7a.  O representante não seleciona a opção "continuar"

1.  O caso de uso volta ao passo 2.


##Requisitos especiais
*

##Lista de variações em tecnologias e dados
* 

##Frequência de Ocorrência
* 

##Questões em aberto
1.	Qual a frequência de ocorrência deste caso de uso?
2.	Quem deve ser notificado do "retirar" das candidaturas?
3.	Será possível reverter o processo de um candidatura retirada?