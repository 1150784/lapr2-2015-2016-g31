﻿##	UC8 - Criar Demonstração

##	Formato breve
O organizador inicia a criação de uma demonstração. O sistema lista as exposições do organizador. O organizador seleciona uma exposição. O sistema lista os recursos disponiveis e solicita a escolha dos recursos necessarios. O organizador escolhe os recursos que deseja. O sistema pede confirmação. O organizador confirma. O sistema pede que insira os dados da demonstração. O organizador insere os dados da demonstração. O sistema pede confirmação. O organizador confirma. O sistema regista as informações e informa do sucesso da operação.

##	SSD de formato breve
![SSD UC8.jpg](UC8.jpg)

##	Formato completo

###Ator principal
+ Organizador

###Partes interessadas e seus interesses
+ Organizador: pretende ter demonstrações apelativas aos representantes.
+ Centro de Exposições: pertende ter as demonstrações devidamente registadas nas exposições.


###Pré-condições
+ O utilizador esta registado como Organizador

###Pós-condições
+ 

###Cenário de sucesso principal (ou fluxo básico)
1. O organizador inicia a criação de uma demonstração.
2. O sistema lista as exposições do organizador.
3. O organizador seleciona uma exposição. 
4. O sistema lista os recursos disponiveis e solicita a escolha dos recursos necessarios.
5. O organizador escolhe os recursos que deseja.
6. O sistema pede confirmação.
7. O organizador confirma. 
8. Os passos 4 a 6 repetem-se ate que todos os recursos necessarios sejam selecionados.
9. O sistema pede que insira os dados da demonstração. 
10. O organizador insere os dados da demonstração. 
11. O sistema pede confirmação. 
12. O organizador confirma. 
13. O sistema regista as informações e informa do sucesso da operação. 

###Extensões (ou fluxos alternativos)
*a. O organisador cancela a operação.

+ O caso de uso encerra 

2a. O organizador não tem exposições atribuidas.

+ O sistema informa da falta de exposições e o caso de uso encerra.

4a. Não ha recursos diponiveis.

+ O sistema informa da falta de recursos e encerra.



##Requisitos especiais
- 

##Lista de variações em tecnologias e dados
-

##Frequência de Ocorrência
-

##Questões em aberto


