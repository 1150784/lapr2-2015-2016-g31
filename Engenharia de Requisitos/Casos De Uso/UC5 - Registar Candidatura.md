# UC5 - Registar candidatura a Exposição #
## Formato Breve ##
O representante do expositor inicia o registo de uma nova candidatura a uma exposição. O sistema apresenta ao representante as exposições que estejam no período de submissão de candidaturas, e solicita a seleção. O representante indica qual a exposição. O sistema solicita os dados necessários à candidatura (nome comercial da empresa, morada, telemóvel, área de exposição pretendida, os produtos a expor e a quantidade de convites a adquirir). O representante do expositor introduz os dados solicitados. O sistema apresenta a lista de demonstrações para a exposição e solicita que indique as que pretende realizar. O sistema valida e apresenta os dados ao representante do expositor, pedindo que os confirme. O representante do expositor confirma os dados da candidatura. O sistema regista a nova candidatura a uma exposição e informa o representante do expositor do sucesso da operação.

## SSD de formato breve ##
![SSD_UC5.png](UC5.png)

## Formato completo##

#### Ator principal ####
* Representante do expositor

#### Partes interessadas e seus interesses ####
* Centro de Exposição: pretende que o processo de registo de uma candidatura seja realizado diretamente pelo representante, através de um meio acessível, evitando a intervenção de um intermediário (do pessoal do Centro de Exposição) e, se possível, diminuir o número de erros associados a esta tarefa.

#### Pré-condições ####
* O Representante da empresa estar registado como utilizador.

#### Pós-condições ####
* A candidatura do expositor à exposição selecionada fica registada no sistema.

##Cenário de sucesso principal (ou fluxo básico) ##
1. O representante do expositor inicia o registo de uma nova candidatura a uma exposição. 
2. O Sistema lista as exposições que estão em período de submissão de candidaturas.
3. O representante do expositor indica qual a exposição à qual se quer candidatar.
4. O sistema solicita os dados necessários à candidatura (nome comercial da empresa, morada, telemóvel, área de exposição pretendida e a quantidade de convites a adquirir).
5. O representante do expositor introduz os dados solicitados.
6. O sistema solicita os dados de um produto a expor (designação do produto).
7. O representante do expositor insere os dados solicitados.
8. Os passos 6 a 7 repetem-se até que todos os produtos a expor tenham sido inseridos.
9. O sistema apresenta a lista de demonstrações para a exposição e solicita que indique as que tem interesse em participar. 
10. O representante indica as que tem interesse em participar.
11. O sistema valida e apresenta os dados da candidatura ao representante do expositor, pedindo que os confirme.
12. O representante do expositor confirma os dados da candidatura.
13. O sistema regista a nova candidatura de um expositor a uma exposição e informa o representante do expositor do sucesso da operação. 

###Extensões (ou fluxos alternativos)###
* a. O representante do expositor solicita o cancelamento da operação. 
>O caso de uso termina.

* 3a. A exposição indicada pelo representante do expositor não é reconhecida.
>1. O sistema informa que a exposição indicada não é reconhecida.
>2. O sistema permite nova introdução da exposição pretendida (passo 3).

* 11b. Dados mínimos obrigatórios relativos à candidatura em falta.
>1. O sistema informa quais os dados em falta.
>2. O sistema permite a introdução dos dados em falta (passo 5).

* 11c. O sistema deteta que os dados (ou algum subconjunto dos dados) relativos à candidatura introduzidos devem ser únicos e que já existem no sistema.
>1. O sistema alerta o representante do expositor para o facto.
>2. O sistema permite a sua alteração (passo 5).

* 11d. O sistema deteta que os dados (ou algum subconjunto dos dados) relativos à candidatura introduzidos são inválidos.
>1. O sistema alerta o representante do expositor para o facto.
>2. O sistema permite a sua alteração (passo 5).
		
#### Requisitos especiais ####
* -
#### Tecnologia e Lista de Variações dos Dados ####
* -
#### Frequência de Ocorrência ####
* -
#### Questões em aberto ####
* Quais são os dados obrigatórios para uma candidatura ser válida?
* Quais os dados que em conjunto permitem detetar a duplicação de candidaturas a uma exposição?
* Alguém deve ser notificado que uma nova candidatura a uma dada exposição foi registada no sistema (e.g. os organizadores)?
* Qual o número mínimo de produtos que um expositor pode inserir?
* Há um número máximo de produtos que um expositor pode incluir na candidatura?
* Qual a frequência de ocorrência deste caso de uso?