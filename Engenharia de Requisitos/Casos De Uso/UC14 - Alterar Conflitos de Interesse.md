# UC14 - Alterar Conflitos de Interesse #

## Formato Breve ##
O FAE inicia a alteração de conflitos de interesse. O sistema mostra a lista de Exposições que a que o FAE está associado. O FAE selecciona a exposição pretendida. O sistema mostra uma lista de candidaturas às quais o FAE tem o encargo de avaliar. O FAE escolhe a candidatura. O sistema mostra uma lista conflito de interesse. O FAE escolhe o conflito de interesse. O sistema mostra os dados do conflito. O FAE altera/remove o conflito. O sistema valida e pede confirmação. O FAE confirma. O sistema regista a informação e informa do sucesso da operação.

## SSD de Formato Breve ##
![SSD UC14.jpg](UC14.jpg)

## Formato Completo ##

### Ator Principal: ###
* FAE

### Partes interessadas e seus interesses: ###
* FAE: pretende que a sua informação esteja correta e não tenha de ser introduzida.

### Pré-condições: ###
* Utilizador registado como FAE.

### Pós-condições: ###
* Os dados do Conflito de interesse alterado são registados.

### Cenário de sucesso principal (ou fluxo básico): ###

1. O FAE inicia a alteração de conflitos de interesse. 
2. O sistema mostra a lista de Exposições que a que o FAE está associado.
3. O FAE selecciona a exposição pretendida. 
4. O sistema mostra uma lista de candidaturas às quais o FAE tem o encargo de avaliar.
5. O FAE escolhe a candidatura. 
6. O sistema mostra uma lista conflito de interesse. 
7. O FAE escolhe o conflito de interesse. 
8. O sistema mostra os dados do conflito. 
9. O FAE altera/remove o conflito. 
10. O sistema valida e pede confirmação. 
11. O FAE confirma. 
12. Repetir desde o passo 6 até o utilizador estar satisfeito.
13. O sistema regista a informação e informa do sucesso da operação.

### Extensões (fluxos alternativos): ###
*a. O FAE solicita o cancelamento da operação.

* O caso de uso termina.

1a. O FAE está fora da data limite definida a alteração de conflitos de interesse.

* O sistema alerta o FAE para o sucedido e o caso de uso termina.

9a. O sistema deteta que o FAE não inseriu uma das duas opções válidas.

1. O sistema alerta o FAE para o facto.
2. O sistema permite a alteração da opção (passo 8).

    2a. O FAE não altera a opção. O caso de uso termina.
    

### Questões em aberto: ###
* Alguma pessoa deve ser notificada da alteração de conflitos de interesse como, por exemplo, os outros FAE da exposição? 
* Qual a frequência de ocorrência deste caso de uso?