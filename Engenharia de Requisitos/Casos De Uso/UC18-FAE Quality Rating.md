﻿# UC18 - FAE Quality Rating #

O Organizador seleciona um FAE. O sistema mostra o FAE escolhido e pergunta se quer listar o rating de qualidades. O utilizador responde sim. O sistema vai apresentar a lista.O Caso de uso termina.

## SSD - Formato Breve ##

## Ator primário ##
* Organizador.

## Partes interessadas e seus interesses ##
* Centro de Exposição: Ter uma forma de listar o rating das qualidades.
* FAE : Será apresentado a sua lista de qualidades

## Pré-condições ##

## Pós-condições ##


## Cenário de sucesso principal (ou fluxo básico) ##
1. O Organizador seleciona um FAE.
2. O sistema pergunta se pretende listar as suas qualidades através do histórico de revisões (de todas as exposições).
3. O Organizador escolhe listar.
4. O sistema apresenta o rank das qualidades em forma de lista .
5. O Caso de uso termina.

## Extensões (ou fluxos alternativos) ##
* *a. O Organizador solicita o cancelamento da operação. 
> 1. O caso de uso termina.

* 2.a Não há nenhum rating de qualidades registado.
>1. O sistema informa o utilizador e o caso de uso termina.


## Requisitos especiais ##
* Não se verificam.

## Lista de variações em tecnologias e dados ##
* Não se verificam.

## Frequência de Ocorrência ##
* Desconhecida.

## Questões em aberto ##
* Isso  é ainda um campo aberto e o centro de exposições não tem ideias claras sobre o que deve ser feito nesta área durante este processo.
* Este assunto deve ser aprofundado com os possíveis centros de exposições e com o cliente através da clarificação de requisitos e possível construção de protótipos.
