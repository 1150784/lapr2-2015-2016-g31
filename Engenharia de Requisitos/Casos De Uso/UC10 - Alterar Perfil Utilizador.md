# UC10 - Alterar Perfil Utilizador #
##	Formato breve
O Utilizador (registado) inicia a alteração do seu perfil. O sistema abre uma interface e apresenta os dados correntes do Utilizador. O Utilizador altera os dados que pretende. O sistema regista os novos dados e confirma o sucesso da operação.

##	SSD de formato breve
![SSD UC10.jpg](UC10.jpg)
 
##	Formato completo

###Ator principal
Utilizador (registado)

###Partes interessadas e seus interesses
+ Centro de exposições: Pretende manter um registo de todos os dados actualizados dos Utilizadores.

###Pré-condições
O Utilizador fazer log-in no sistema.

###Pós-condições
+ Ter um registo actualizado de todos os Utilizadores.

###Cenário de sucesso principal (ou fluxo básico)
1. O Utilizador solicita a alteração do seu perfil.
2. O sistema abre uma interface e apresenta os dados correntes do Utilizador. 
3. O Utilizador selecciona (apenas uma escolha) a informação que pretende alterar.
4. O sistema apresenta uma janela em que solicita a nova informação.
5. O Utilizador faz a alteração à informação que seleccionou.
6. O sistema compara a informação anterior e a nova informação e solicita uma confirmação.
7. O Utilizador confirma a alteração.
8. O sistema regista os novos dados e informa o Utilizador do sucesso da operação.
9.  Repetir passo 3 até o Utilizador fazer todas as alterações que achar necessário.
10. O Utilizador pede para guardar as alterações feitas ao seu perfil.
11. O sistema guarda as alterações do Utilizador e termina a alteração do perfil.

###Extensões (ou fluxos alternativos)
*a. O Gestor de Exposições solicita cancelamento da definição do recurso.

+ O caso de uso termina.

3a. O Utilizador selecciona a alteração da password.

1.  O sistema apresenta uma janela que solicita a password antiga, a nova password e uma confirmação da nova password.
2.  O Utilizador preenche os campos com os respectivos dados.
3.  O sistema solicita confirmação.
4.  O Utilizador confirma a alteração.
5.  O sistema regista os novos dados e informa o Utilizador do sucesso da operação.
6. (passo 2)

5a. A informação inserida é inválida.

1. O sistema informa o Utilizador que os dados inseridos não são válidos.
2.  (passo 4)

7a. O Utilizador não confirma a alteração.

1.  O sistema informa o utilizador que não foram guardadas as alterações.
2.  (passo 2)

##Questões em aberto
+ O Utilizador deve ser informado, através do e-mail, que foram feitas alterações ao seu perfil? 
+ É possível editar todos os dados, incluindo username?
+ Deverá haver um intervalo de tempo obrigatório para ser possível alterar algum dado (ou todos) novamente?