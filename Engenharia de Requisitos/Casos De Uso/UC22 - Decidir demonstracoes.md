﻿# UC22 - Decidir demonstrações #

O Organizador de exposições inicia no sistema o processo de decisão de demonstrações.  O sistema lista todas as demonstrações. O Organizador escolhe as demonstrações que funcionarão. O sistema pede para definir um período de tempo. O organizador define um período de candidatura às demonstrações.  O sistema pede confirmação. O organizador confirma. 

## SSD - Formato Breve ##

![uc22.jpg](https://bitbucket.org/repo/8aRMng/images/633869972-uc22.jpg)

## Ator primário ##
* Organizador de Exposições. 

## Partes interessadas e seus interesses ##

* Organizador de Exposições : Pretende definir as demonstrações.


## Pré-condições ##


## Pós-condições ##

* Os dados serem guardados no sistema.

## Cenário de sucesso principal (ou fluxo básico) ##
1. O Organizador de exposições inicia no sistema o processo de decisão de demonstrações.
2. O sistema lista todas as demonstrações.
3. O Organizador escolhe as demonstrações que funcionarão.
4. O sistema pede para definir um período de tempo.
5. O organizador define um período de candidatura às demonstrações.
6. O sistema pede confirmação.
7. O organizador confirma.
8. o sistema informa do sucesso.

## Extensões (ou fluxos alternativos) ##
* *a. O Gestor de Exposições solicita o cancelamento da operação. 
> 1. O caso de uso termina.

* 2.a Não há demonstrações no sistema.
>1. O sistema informa o utilizador e o caso de uso termina.

* 3.a O organizador não escolhe nenhuma demonstração.
>1. O sistema informa o organizador
>2. O passo 3 repete

* 5.a O organizador define um período de candidatura às demonstrações.
>1. O sistema informa o organizador
>2. O passo 5 repete

## Requisitos especiais ##
* Não se verificam.

## Lista de variações em tecnologias e dados ##
* Não se verificam.

## Frequência de Ocorrência ##
* Desconhecida.

## Questões em aberto ##
* O organizador poderá depois alterar as demonstrações escolhidas? 



