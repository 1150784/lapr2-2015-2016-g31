﻿##	UC4 - Avaliar Candidatura

##	Formato breve
O FAE inicia no sistema o processo de avaliação sobre candidaturas. O sistema mostra uma lista de exposições em que o utilizador FAE é um dos avaliadores. O FAE seleciona uma exposição. O sistema mostra uma lista de candidaturas que requerem uma avaliação do FAE. O FAE escolhe uma candidatura. O sistema mostra a informação (...) que caracteriza a candidatura e solicita a indicação se a candidatura deve ser aceite ou recusada. O FAE indica no sistema se a candidatura é aceite ou recusada e introduz uma breve texto justificativo. O sistema valida a avaliação e pede ao FAE confirme a avaliação como definitiva. O FAE confirma. O sistema regista a decisão tomada. Os passos 4 a 10 repetem-se até que todas as candidaturas tenham sido tratadas.  O sistema informa o utilizador que o caso de uso termina e as avaliações ficaram registadas.

##	SSD de formato breve
![SSD UC4.jpg](UC4.jpg)

##	Formato completo

###Ator principal
FAE

###Partes interessadas e seus interesses
+ Expositor: Pretende ter informação acerca de quais candidaturas foram aceites.
+ FAE: Ter aprovação na sua decisão.
+ Centro de exposições: Pretende ter exposições com conteudo selecionado por entidades competentes.

###Pré-condições
+ Utilizador autenticado no sistema como FAE.

###Pós-condições
+ As decisões tomadas pelo FAE são registadas.

###Cenário de sucesso principal (ou fluxo básico)
1. O FAE inicia no sistema o processo de avaliação sobre candidaturas.
2. O sistema mostra uma lista de exposições em que o utilizador FAE é um dos avaliadores.
3. O FAE seleciona uma exposição.
4. O sistema mostra uma lista de candidaturas que requerem uma avaliação do FAE.
5. O FAE escolhe uma candidatura.
6. O sistema mostra a informação (...) que caracteriza a candidatura e solicita a indicação se a candidatura deve ser aceite ou recusada.
7. O FAE indica no sistema se a candidatura é aceite ou recusada e introduz uma breve texto justificativo.
8. O sistema valida a avaliação e pede ao FAE confirme a avaliação como definitiva.
9. O FAE confirma.
10. O sistema regista a decisão tomada.
11. Os passos 4 a 10 repetem-se até que todas as candidaturas tenham sido tratadas.
12. O sistema informa o utilizador que as avaliações ficaram registadas e o caso de uso termina.

###Extensões (ou fluxos alternativos)
*a. A qualquer momento o FAE cancela o processo.

+ O caso de uso termina.

2a. O utilizador não é FAE de nenhuma exposição.

+ O sistema informa o utilizador e o caso de uso termina.

4a. Não ha nenhuma candidatura atribuida ao FAE.

+ O sistema informa o utilizador e o caso de uso encerra.




##Requisitos especiais
- 

##Lista de variações em tecnologias e dados
-

##Frequência de Ocorrência
-

##Questões em aberto


