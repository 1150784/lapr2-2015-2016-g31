﻿# UC11 - Alterar Candidatura #
##	Formato breve
O representante do expositor inicia a alteração de candidaturas. O sistema apresenta uma lista das candidaturas do representante. O representante escolhe a candidatura que pretende alterar. O sistema apresenta os dados da candidatura. O representante altera os dados que necessita e guarda a candidatura. O sistema valida a alteração e guarda-a.

##	SSD de formato breve
![SSD UC11.jpg](UC11.jpg)
 
##	Formato completo

###Ator principal
* Representante do expositor

###Partes interessadas e seus interesses
+ Centro de exposições: Prentende poder alterar os dados de uma candidatura ser ter de criar uma nova.
+  Representante da empresa: Pode aplicar mudanças à sua candidatura depois de a ter criado, dando mais marem de manobra.

###Pré-condições
+ A candidatura Já foi criada e registada.

###Pós-condições
+ 

###Cenário de sucesso principal (ou fluxo básico)
1. O representante do expositor inicia a alteração de candidaturas.
2. O sistema apresenta uma lista das candidaturas do representante. 
3. O representante escolhe a candidatura que pretende alterar. 
4. O sistema apresenta os dados da candidatura. 
5. O representante altera os dados que necessita e guarda a candidatura. 
6. O sistema valida as alterações.
7. O sistema regista as alterações.

###Extensões (ou fluxos alternativos)
*.a O representante do expositor solicita o cancelamento da operação. 

+ O sistema encerra o caso de uso.

3.a A candidatura já não se encontra dentro da data limite.

+ O sistema informa o representante que já não é possivel alterar a candidatura e encerra.

##Questões em aberto
