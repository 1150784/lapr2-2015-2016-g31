# UC9 - Definir Recurso #
##	Formato breve
O Gestor de Exposições inicia a definição do recurso (e.g. água, electricidade, exaustão de ar). O sistema pede ao Gestor de Exposições que insira os dados relativos ao recurso em causa. O Gestor de Exposições insere os dados (e.g. nome, descrição e consumo). O sistema guarda os dados e informa o Gestor de Exposições sobre o sucesso da operação.

##	SSD de formato breve
![SSD UC9.jpg](UC9.jpg)
 
##	Formato completo

###Ator principal
Gestor de Exposições

###Partes interessadas e seus interesses
+ Centro de exposições: Pretende manter registo de todos os dados de todas as candidaturas para todas as exposições.
+ Representante da empresa: Pretende saber todos os detalhes de um recurso.

###Pós-condições
+ Ter uma lista detalhada de todos os recursos que irão ser usados nas demonstrações.

###Cenário de sucesso principal (ou fluxo básico)
1. O Gestor de Exposições solicita a definição de recursos.
2. O sistema apresenta todos os dados já existentes na base de dados.
3. O Gestor de Exposições inicia a criação de um novo recurso.
4. O sistema solicita os dados.
5. O Gestor de Exposições insere todos os dados necessários acerca do recurso a ser utilizado.
6. O sistema valida os dados e solicita confirmação.
7. O Gestor de Exposições confirma.
8. O sistema guarda os dados.
9. São repetidos os passos 3-7 até o Gestor de Exposições estar satisfeito com a operação.
10. O sistema informa o Gestor de Exposições sobre o sucesso da operação.

###Extensões (ou fluxos alternativos)
*a. O Gestor de Exposições solicita cancelamento da definição do recurso.

+ O caso de uso termina.

2a. Não existem ainda recursos criados.

+ É criada uma nova lista e o caso de uso prossegue.

6a. Dados mínimos obrigatórios em falta.

1. O sistema informa quais os dados em falta.
2. O sistema permite a introdução dos dados em falta (passo 3)

    2.a O Gestor de Exposições não altera os dados. O caso de uso termina.

6b. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

1. O sistema alerta o utilizador para o facto.
2. O sistema permite a sua alteração (passo 3)
    
    2a. O Gestor de Exposições não altera os dados. O caso de uso termina.

6c. O sistema detecta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.

1. O sistema alerta o utilizador para o facto.
2. O sistema permite a sua alteração (passo 3)

    2a. O Gestor de Exposições não altera os dados. O caso de uso termina.

##Questões em aberto
+ A lista de recursos tem um limite?
+ Existirá já previamente uma lista com nomes exactos para os recursos ou o utilizador poderá dar o nome que lhe for conveniente?
+ A criar um novo recurso, haverão atributos para cada recurso? Se sim, quais?
+ Poderão existir recursos repetidos mas com atributos diferentes?