# UC15 - Criar Stand #

O Gestor de Exposições inicia a criação de um Stand. O sistema solicita os dados do Stand. O Gestor de Exposições introduz os dados do Stand. O sistema solicita confirmação dos dados inseridos. O Gestor de Exposições confirma os dados. O sistema valida e regista o Stand e informa o Gestor de Exposições do sucesso da operação.

## SSD - Formato Breve ##
![SSD_UC15.png](UC15.jpg)

## Ator primário ##
* Gestor de Exposições.

## Partes interessadas e seus interesses ##
* Centro de Exposições: Pretende ter um registo de todos os Stands disponíveis no sistema.

## Pós-condições ##
* O Stand criado fica registado no sistema.

## Cenário de sucesso principal (ou fluxo básico) ##
1. O Gestor de Exposições inicia a criação de um stand.
2. O Sistema solicita os dados (descrição) do Stand.
3. O Gestor de Exposições introduz os dados do stand.
4. O Sistema valida e pede confirmação dos dados do stand.
5. O Gestor de Exposições confirma os dados do Stand.
6. Os passos 2 a 5 são repetidos até que todos os stands estejam criados.
6. O sistema regista os dados e informa o Gestor de exposições do sucesso da operação.

## Extensões (ou fluxos alternativos) ##
* *a. O Gestor de Exposições solicita o cancelamento da operação. 
> 1. O caso de uso termina.
* 3a. O Gestor de Exposições não introduz os dados obrigatórios.
> 1. O sistema informa quais são os dados em falta.
> 2. O sistema permite a introdução dos dados que estão em falta.
>> 2a. O Gestor de exposições não insere os dados e o caso de uso termina.

* 3b. O sistema deteta que o stand já se encontra criado.
>1. O sistema alerta o Gestor de exposições e o caso de uso continua no passo 2.
>> 1a. O Gestor de exposições não fornece os dados sobre o stand e o caso de uso termina.

## Requisitos especiais ##
* Não se verificam.

## Lista de variações em tecnologias e dados ##
* Não se verificam.

## Frequência de Ocorrência ##
* Desconhecida.

## Questões em aberto ##
* O que acontece se não houver stands criados no Centro de Exposições?
* Como deve o sistema validar a descrição do stand?
