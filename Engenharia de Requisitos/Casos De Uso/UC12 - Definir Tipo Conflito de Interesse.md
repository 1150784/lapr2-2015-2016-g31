﻿# UC12 - Definir Tipo de Conflito de Interesse #
##	Formato breve
O gestor de exposições inicia a defenição dos tipos de conflito de interesse. O sistema apresenta todas exposições. O gestor de exposições indica a exposição que pretende usar. O sistema pede a introdução do tipo de conflito. O gestor de exposições insere o tipo de conflito. O sistema valida os dados introduzidos e pede confirmação. O administrador confirma. O sistema regista o conflito de interesse. 
##	SSD de formato breve
![SSD UC12.jpg](UC12.jpg)
 
##	Formato completo

###Ator principal
* Gestor de Exposições

###Partes interessadas e seus interesses
+ Gestor de Exposições: Tenciona tornar a defenição dos conflitos de interesse rapida e eficaz.
+ FAE: Necessitam da lista dos conflitos de interesse.

###Pré-condições
+ 

###Pós-condições
+ 

###Cenário de sucesso principal (ou fluxo básico)
1. O gestor de exposições inicia a defenição dos tipos de conflito de interesse. 
2. O sistema apresenta todas exposições. 
3. O gestor de exposições indica a exposição que pretende usar. 
4. O sistema pede a introdução do tipo de conflito.
5. O gestor de exposições insere o tipo de conflito.
6. O sistema valida os dados introduzidos e pede confirmação.
7. O Gestor confirma.
8. O sistema regista o conflito de interesse.
9. Repete os passos 4 a 7 ate o gestor decidir continuar.
10. Repete os passos 3 a 7 ate o gestor decidir parar.

###Extensões (ou fluxos alternativos)
*.a O representante do expositor solicita o cancelamento da operação. 

+ O sistema encerra o caso de uso.


##Questões em aberto
+ Quais os tipos de dados que o conflito de interesses deve ter?