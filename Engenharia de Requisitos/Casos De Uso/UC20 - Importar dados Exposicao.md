# UC20 - Importar dados Exposição #

O organizador inicia a importação de dados de uma determinada exposição a partir de um ficheiro. O sistema solitica a localização do ficheiro. O organizador insere a localização do ficheiro. O sistema lê o ficheiro, valida, mostra a informação e pede confirmação. O organizador confirma os dados. O sistema importa toda a informação.

## SSD - Formato Breve ##
![SSD_UC20.png](UC20.jpg)

## Ator primário ##
* Organizador

## Partes interessadas e seus interesses ##
* Centro de Exposições: Pretende carregar para o sistema dados um exposição anterior de forma a auxiliar o organizador numa exposição atual de forma a aumentar a produtividade.
* Organizador: Pretende ter acesso a informação de uma exposição anterior.

## Pós-condições ##
* Os dados de uma Exposição localizados num ficheiros ficam registados no sistema.

## Cenário de sucesso principal (ou fluxo básico) ##
1. O Organizador inicia a importação de dados de uma exposição a partir de um ficheiro.
2. O Sistema solicita a localização do ficheiro com os dados da exposição.
3. O Organizador introduz a localização do ficheiro.
4. O Sistema lê o ficheiro, valida, mostra ao organizador a informação e pede confirmação.
5. O Organizador confirma os dados.
6. O Sistema regista a informação do ficheiro e informa do sucesso da operação.

## Extensões (ou fluxos alternativos) ##
* *a. O Organizador solicita o cancelamento da operação. 
> 1. O caso de uso termina.

* 3a. O Gestor de Exposições não introduz dados válidos.
> 1. O sistema informa do sucedido.
> 2. O sistema permite a introdução de uma nova localização.
>> 2a. O Gestor de exposições não insere os dados e o caso de uso termina.

* 4a. O sistema deteta que a exposição a importar já se encontra no sistema.
> 1. O Sistema informa do sucedido.
> 2. O sistema permite a introdução de uma nova localização.
>> 2a. O organizador não insere uma nova localização. O caso de uso termina.

## Requisitos especiais ##
* Não se verificam.

## Lista de variações em tecnologias e dados ##
* Não se verificam.

## Frequência de Ocorrência ##
* Desconhecida.

## Questões em aberto ##
* Deve permitir várias exposições de um ficheiro?
