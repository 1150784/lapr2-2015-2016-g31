# UC13 - Detetar Conflitos #
##	Formato breve
O tempo, quando o período de submissão de candidaturas de uma dada exposição termina, desencadeia o processo de detetação de conflitos de interesse entre os FAE e candidaturas atribuidas a estes.

##	SSD de formato breve
![SSD UC13.jpg](UC13.jpg)
 
##	Formato completo

###Ator principal
Tempo

###Partes interessadas e seus interesses
+ Centro de exposições: Pretende que haja um mecanismo de detetação de conflitos de interesse na avaliação de candidaturas automático de forma a melhorar o processo de avaliação das candidaturas.

###Pré-condições
+ Não se verificam.

###Pós-condições
+ Registo de uma lista de conflitos de interesse na avaliação de candidaturas detetados pelo sistema.

###Cenário de sucesso principal (ou fluxo básico)
1. Quando a data de fim do período de submissão de uma exposição chega ao fim o sistema inicia a deteção de conflitos.
2. O sistema verifica se estão reunidas as condições para a deteção de conflitos para determinada exposição.
3. Para cada atribuição verifica a existência de todos os tipos de conflitos que estejam definidos no sistema e regista os conflitos detetados.
5. O sistema assinala o fim da deteção de conflitos.

###Extensões (ou fluxos alternativos)
+ Não se verificam.

###Requisitos especiais
+ Não se verificam.

###Tecnologia e Lista de Variações dos Dados
+ Não se verificam.

###Frequência de Ocorrência
+ Desconhecida

##Questões em aberto
+ Existe limite máximo de conflitos detetados?
+ Alguém deve ser notificado da conclusão da operação?