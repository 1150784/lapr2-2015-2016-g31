﻿# UC17 - Keyword Ranking #

O Gestor de Exposições inicia o ranking das keywords. O sistema apresenta o rank das keywords em forma de lista e qestiona o utilizador se deseja criar um grafico. O utilizador responde sim. O sistema cria o grafico e apresenta-o.O Caso de uso termina.

## SSD - Formato Breve ##
![SSD_UC17.png](UC17.jpg)

## Ator primário ##
* Gestor de Exposições.

## Partes interessadas e seus interesses ##
* Centro de Exposição: Ter uma forma de listar as keywords.
* Gestor de Exposições : Tenciona ter uma maneira de ver quais são as keywords mais usadas e de que tipo de submissão provêm.


## Pré-condições ##

## Pós-condições ##


## Cenário de sucesso principal (ou fluxo básico) ##
1. O Gestor de Exposições inicia o ranking das keywords.
2. O sistema apresenta o rank das keywords em forma de lista e pede confirmação.
3. O Gestor confirma
4. O sistema cria um ficheiro csv e termina o caso de uso.

## Extensões (ou fluxos alternativos) ##
* *a. O Gestor de Exposições solicita o cancelamento da operação. 
> 1. O caso de uso termina.

* 2.a Não há keywords registadas.
>1. O sistema informa o utilizador e o caso de uso termina.


## Requisitos especiais ##
* Não se verificam.

## Lista de variações em tecnologias e dados ##
* Não se verificam.

## Frequência de Ocorrência ##
* Desconhecida.

## Questões em aberto ##
* 

