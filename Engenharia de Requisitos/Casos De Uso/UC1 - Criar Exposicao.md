﻿#     UC1 - Criar Exposição
##	Formato breve
O Gestor de Exposições inicia a criação de uma exposição. O sistema pede os dados (titulo, descrição, data de início, data de fim, local e responsáveis) da exposição. O Gestor de Exposições insere os dados da exposição. O sistema valida e e guarda a nova exposição.

##	SSD de formato breve
![SSD UC1.jpg](UC1.jpg)
 
##	Formato completo

###Actor principal
Gestor de Exposições

###Partes interessadas e seus interesses
+ Gestor de exposições: Pretende criar as exposições.
+ Centro de exposições: Pretende manter um registo das exposições criadas.

###Pós-condições
+ A Exposição fica registada no sistema.

###Cenário de sucesso principal (ou fluxo básico)
1. O Gestor de Exposições inicia o caso de uso.
2. O sistema solicita os dados necessários para a criação de uma Exposição(título, texto descritivo, data início, data fim, local de realização. período de submissão de candidaturas e data limite de especificação de conflitos ).
3. O Gestor de Exposições introduz os dados solicitados.
4. O sistema valida e mostra uma lista de utilizadores (para escolher os Organizadores).
5. O Gestor de Exposições selecciona um utilizador.
6. O sistema apresenta os dados do utilizador.
7. O Gestor de Exposições define-o como Organizador.
8. Repetir passos 4-7 e repetir as vezes que o Gestor de Exposições desejar(minimo 2 vezes).
9. O sistema valida novamente os dados e solicita uma confirmação.
10. O Gestor de Exposições confirma todos os dados.
11. O sistema cria a exposição e avisa o Gestor de Exposições do sucesso da operação.

###Extensões (ou fluxos alternativos)
*a. O Gestor de Exposições solicita cancelamento do registo.

+ O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.

1. O sistema informa quais os dados em falta.
2. O sistema permite a introdução dos dados em falta (passo 3)

    2a. O Gestor de Exposições não altera os dados. O caso de uso termina.


4b. O sistema detecta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.

1. O sistema alerta o Gestor de Exposições para o facto.
2. O sistema permite a sua alteração (passo 3)

    2a. O Gestor de Exposições não altera os dados. O caso de uso termina.

4c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

1. O sistema alerta o Gestor de Exposições para o facto.
2. O sistema permite a sua alteração (passo 3)
    
    2a. O Gestor de Exposições não altera os dados. O caso de uso termina.

4d. O lista de utilizadores está vazia.

+ O caso de uso termina.

5a. O utilizador seleccionado já tem um cargo de FAE na exposição.

1. O sistema pede para seleccionar outro utilizador.
2. O sistema mostra a lista de utilizadores. 

8a. O Gestor de Exposições selecciona apenas um utilizador como Organizador.

1. O sistema informa o Gestor de Exposições que terá de seleccionar pelo menos mais um Organizador para poder prosseguir.
2. O sistema mostra a lista de utilizadores.


##Requisitos especiais
- 

##Lista de variações em tecnologias e dados
-

##Frequência de Ocorrência
-

##Questões em aberto
+ Quais são os dados obrigatórios para a criação de novas exposições?
+ Alguém deve ser notificado da criação de uma exposição como, por exemplo, o Organizador?
+ Qual a frequência de ocorrência deste caso de uso?
+ Qual o tipo de artigos que vão poder ser expostos?
+ Haverá limite para artigos expostos por espaço?
+ Poderá haver espaços diferentes com o mesmo tipo de artigos expostos?
+ Organizador deve ter uma cópia dos atributos do seu Utilizador ou apenas deve ter uma referência ao tal Utilizador?