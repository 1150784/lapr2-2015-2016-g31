**Análise das candidaturas:** Processo em que um FAE analisa a candidatura e aceita ou rejeita a candidatura;

**Área de exposição:** Área em metros quadrados que ocupa o stand do expositor;

**Artigo:** Material ou produto que é exposto;

**Camel case:** Convenção de nomenclatura em que a primeira letra de cada palavra numa palavra composta é maiúscula.

**Candidatura:** Pedido efectuado pelo representante do expositor para expor os seus artigos;

**Conflito de interesse:** Situação que pode condicionar o FAE com determinada candidatura.

**Credencial de acesso:** Informação usada pelo utilizador para aceder ao sistema (login);

**Centro de exposições:** Entidade que pretende adquirir o software;

**Dados da Exposição:** Informações relativa a uma exposição;

**Dados do Utilizador:** Informação relativa a um utilizador;

**Demonstração:** Evento dentro da exposição onde o expositor pode expor produtos de um tipo específico.

**Distribuição das candidaturas:** Processo em que um organizador atribui um FAE uma quantidade de candidaturas para posterior análise:

**Exposição:** Demonstração de artigos de várias empresas ao público durante um determinado período tempo;

**Expositor:** Empresa/Representante que expõe os produtos;

**FAE:** Sigla que significa "Funcionário de Apoio à Exposição". Utilizador Registado;

**Gestor de exposição:** Pessoa responsável pela criação e gestão de todas as exposições do centro de exposição e por definir um ou mais organizadores. Utilizador Registado;

**Local de realização:** Local onde a exposição tem lugar;

**Mecanismo de Atribuição:** Processo automático que permite distribuir as diferentes candidaturas de uma dada exposição por todos os FAE dessa mesma exposição.

**Mecanismos de Detecção:** Algoritmo/Regra de detecção de conflitos baseado em vários critérios.

**Organizador:** Pessoa que é responsável pela organização de uma exposição. Utilizador Registado;

**Perfil do utilizador:** Informação relativa ao utilizador;

**Produto:** Material ou artigo que é exposto;

**Recurso:** Bem material usado nas demonstrações.

**Registo de utilizador:** Submissão e envio de informação acerca do utilizador de forma a obter acesso ao sistema;

**Representante do expositor:** Pessoa responsável pela submissão da candidatura à exposição. Utilizador Registado;

**Stand**: Espaço físico na exposição atribuída a cada expositor (empresa);

**Tipo de Conflito:** Situação definidas pelo gestor de exposições que pode condicionar o FAE com determinada candidatura.**

**Utilizador Registado:** Representação de uma pessoa que utiliza o sistema;

**Utilizador não registado:** Representação de uma pessoa sem registo no sistema;
