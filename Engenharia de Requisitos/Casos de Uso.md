# Diagrama de Casos de Uso #



# Casos de Uso
+ [UC1: Criar exposição](Casos De Uso/UC1 - Criar Exposicao)
+ [UC2: Definir FAE](Casos De Uso/UC2 - Definir FAE)
+ [UC3: Atribuir candidatura](Casos De Uso/UC3 - Atribuir Candidatura)
+ [UC4: Avaliar Candidatura](Casos De Uso/UC4 - Avaliar Candidatura)
+ [UC5: Registar candidatura](Casos De Uso/UC5 - Registar Candidatura)
+ [UC6: Registar utilizador](Casos De Uso/UC6 - Registar Utilizador)
+ [UC7: Confirmar Registo de Utilizadores](Casos De Uso/UC7 - Confirmar novo utilizador)
+ [UC8: Criar Demonstração](Casos De Uso/UC8 - Criar Demonstracao)
+ [UC9: Definir Recurso](Casos De Uso/UC9 - Definir Recurso)
+ [UC10 : Alterar Perfil Utilizador](Casos De Uso/UC10 - Alterar Perfil Utilizador)
+ [UC11: Alterar Candidatura](Casos De Uso/UC11 - Alterar Candidatura)
+ [UC12: Definir Tipo Conflito de Interesse](Casos De Uso/UC12 - Definir Tipo Conflito de Interesse)
+ [UC13: Detetar Conflitos](Casos De Uso/UC13 - Detetar Conflitos)
+ [UC14: Alterar Conflitos de Interesse](Casos De Uso/UC14 - Alterar Conflitos de Interesse)
+ [UC15: Criar Stand](Casos De Uso/UC15 - Criar Stand)
+ [UC16: Taxa de aceitação](Casos De Uso/UC16 - Taxa de aceitacao)
+ [UC17: Keyword Rating](Casos De Uso/UC17-Keyword Rating)
+ [UC18: FAE Quality Rating](Casos De Uso/UC18-FAE Quality Rating)
+ [UC19: Retirar Candidatura](Casos De Uso/UC19-Retirar Candidatura)
+ [UC20: Importar dados Exposição](Casos De Uso/UC20 - Importar dados Exposicao)
+ [UC21: Atribuir Stands](Casos De Uso/UC21-Atribuir Stands)
+ [UC22: Decidir demonstrações](Casos De Uso/UC22 - Decidir demonstracoes)
+ [UC23: Confirmar Stand](Casos De Uso/UC23 - Confirmar Stand)
