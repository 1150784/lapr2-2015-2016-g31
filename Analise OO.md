#	Análise OO
O processo de construção do modelo de domínio é baseado nos casos de uso, em especial os substantivos utilizados, e na descrição do enunciado.
##	Racional para identificação de classes de domínio
Para a identificação de classes de domínio usa-se a lista de categorias das aulas TP (sugeridas no livro). Como resultado temos a seguinte tabela de conceitos (ou classes, mas não de software) por categoria: 

Categoria: Conceitos

Categorias | Conceitos de Negócio... | 
---------- | ----------------------- | 
Transações  (do negócio): | Candidatura; Avaliação de Candidatura; Atribuição de Candidatura
Linhas de transações |
Produtos ou serviços relacionados com transações: |Produto
Registos (de transações): |
Papéis das pessoas: |Gestor de Exposições; Organizador; FAE; Representante; Utilizador
Lugares: |Centro de Exposição
Eventos: | Exposição
Objetos físicos: |
Especificações e descrições: |Mecanismo de Atribuição, Mecanismo de Detecção de Conflitos
Catálogos: |
Conjuntos (containers?): |Lista de Demonstrações, Lista de Recursos, Lista de Tipos de Conflito
Elementos de conjuntos: |
Organizações: | Expositor
Outros sistemas (externos): | 
Registos (financeiros), de trabalho, contractos, documentos legais: |
Instrumentos financeiros: 
Documentos referidos/para executar as tarefas: |


##	Racional sobre identificação de associações entre classes
Uma associação é uma relação entre instâncias de objectos que indica uma conexão relevante e que vale a pena recordar, ou é derivável da Lista de Associações Comuns: 
+	A é fisicamente (ou logicamente) parte de B
+	A está fisicamente (ou logicamente) contido em B
+	A é uma descrição de B
+	A é conhecido/capturado/registado por B
+	A usa ou gere B
+	A está relacionado com uma transacção de B
+	etc.

Conceito (A) | Associaçao | Conceito (B) |
------------ | -----------| -------------|
Avaliação | feita pelo |FAE
          | é de | Candidatura
Atribuição | feita pelo | Organizador
           |relativa a | Candidatura
           |usa | Mecanismos de atribuição
           |gera | Avaliação
Centro de Exposições | organiza | Exposição
                     | tem registo | Utilizador
                     | possui | Recursos
Candidatura | acerca de |Expositor
            | para expor | Produto
            | feita pelo | Representante
            | a uma | Exposição
            |tem | Avaliação
Conflito | de | FAE
             | especificado por | FAE
             | adicionado / removido por | FAE
             | tem | Tipo de Conflito
             | é detectado por | Mecanismo Deteção de Conflitos
Exposição | tem | FAE
          | tem | Avaliação
          | tem | Demonstrações
          | recebe  | Candidatura
          | é criada pelo | Gestor de Exposições
          | organizada | Centro de Exposições
FAE | tem papel de | Utilizador
    | recebe | Atrbuição
    | especifica | Conflito
    | adiciona / remove | Conflito
Gestor de Exposição | cria | Exposição
                    | define | Organizador
                    | tem papel de | Utilizador
                    | define | Lista de Recursos
Mecanismo Deteção de Conflitos | detecta | Conflito
                                   | para | Tipo de Conflito
Organizador | define | FAE
            |despoleta | Atribuições
            |define | Lista de Demonstrações
            | tem papel de | Utilizador
            | organiza | Exposição
Representante |faz |Candidatura
              | escolhe| Demonstrações
              | tem papel de | Utilizador
Tipos de Conflito | definido por | Gestor de Exposições
                      | existe | Mecanismo Deteção de Conflitos
Utilizador | pode ser  | FAE
           | pode ser  |Organizador
           | pode ser | Gestor de Exposições
           | pode ser | Representante

##	Diagrama de MD
![MD.png](md.jpg)

## Diagrama de Estados

### Exposição

![CandidaturaState.png](ExposicaoEstado.jpg)

### Candidatura
![CandidaturaState.png](CandidaturaEstado.jpg)