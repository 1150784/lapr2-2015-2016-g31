﻿# Realização do UC11 Alterar candidatura #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O representante do expositor inicia a alteração de candidaturas. | quem controla a alteração das candidaturas? | AlterarCandidaturaController | Controller
2. O sistema apresenta uma lista das candidaturas do representante. | n/a |  |
3. O representante escolhe a candidatura que pretende alterar. | quem guarda as candidaturas | RegistoCandidaturas
4. O sistema apresenta os dados da candidatura.  | n/a | |
5. O representante altera os dados que necessita e guarda a candidatura.  | quem guarda as alterações introduzidas?|RegistoCandidatura |
6. O sistema valida as alterações. | Quem faz a validação dos dados? | RegistoCandidatura | Tem os dados da candidatura
7. O sistema regista as alterações.| Quem guarda as alterações? | RegistoCandidatura |

## Sistematização: ##
Do racional resulta que as classes conceptuais promovidas a classes de software são:

* Exposição

* Candidatura

Outras classes de software (i.e. Pure Fabrication) identificadas:

* AlterarCandidaturaUI

* AlterarCandidaturaController
 

## Diagrama de Sequência ##

![UC9-DS](UC9-DS.jpg)

## Diagrama de Classes ##

![UC9-DC](UC9-DC.jpg)