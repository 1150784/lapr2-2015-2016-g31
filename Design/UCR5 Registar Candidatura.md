# Realização do UC5 Registar Candidatura a Exposição #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O representante do expositor inicia o registo de uma nova candidatura a uma exposição.   |n/a||
2. O Sistema lista as exposições que estão em período de submissão de candidaturas. | ... conhece todas as exposições? | RegistoExposicoes | RegistoExposicoes mantém registo de todas as exposições.
3. O representante do expositor indica qual a exposição à qual se quer candidatar. | n/a ||
4. O sistema solicita os dados necessários à candidatura (nome comercial da empresa, morada, telemóvel, área de exposição pretendida e a quantidade de convites a adquirir). | ... guarda os dados da candidatura? | Candidatura |
|| ... cria a nova Candidatura? | ListaCandidaturasExposicao |
|| ... cria a novo Expositor? | Candidatura |
5. O representante do expositor introduz os dados solicitados. | n/a ||
6. O sistema solicita os dados de um produto a expor (designação do produto). | ... guarda os dados do produto? |Produto |
||... cria o novo Produto? | ListaProdutos
7. O representante do expositor insere os dados solicitados. | ... guarda o novo Produto? | ListaProdutos|
|| ... valida o novo Produto? | ListaProdutos
8. Os passos 6 a 7 repetem-se até que todos os produtos a expor tenham sido inseridos. | n/a ||
9. O sistema apresenta a lista de demonstrações para a exposição e solicita que indique as que tem interesse em participar.  | ... conhece as demonstrações na exposição? | ListaDemonstracoes | A classe ListaDemonstracoes mantém registo de todas as demonstrações da exposição.
10. O representante indica as que tem interesse em participar. | ... guarda as demonstrações para a candidatura?  | Candidatura  | IE: a Candidatura tem Demonstrações.
11. O sistema valida e apresenta os dados da candidatura ao representante do expositor, pedindo que os confirme. |... valida os dados?|Exposição|IE: Validação global (e.g. duplicados)
||| ListaCandidaturas | IE: Validação local
12. O representante do expositor confirma os dados da candidatura. |n/a||
13. O sistema regista a nova candidatura de um expositor a uma exposição e informa o representante do expositor do sucesso da operação.  |... regista a nova Candidatura criada?|ListaCandidaturas|
|| ... tem ListaCandidaturas? | Exposição

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* CentroExposicoes  
* Exposição  
* Candidatura
* Expositor
* Produto 

Por high cohesion e low coupling surgem mais classes de software:
* ListaCandidaturas
* ListaDemonstracoes
* ListaProdutos
* RegistoExposicoes

Outras classes de software (i.e. Pure Fabrication) identificadas:  

* RegistarCandidaturaUI
* RegistarCandidaturaController


## Diagrama de Sequencia ##

![UC5.jpg](UC5-DS.jpg)


## validaEAdicionaCandidatura ##

![addCandidatura.jpg](UC5-DS2.jpg)


## Diagrama de Classes

![UC5-DC.jpg](UC5-DC.jpg)