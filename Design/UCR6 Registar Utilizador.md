# Realização do UC6 Registar utilizador #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Utilizador (não registado) inicia no sistema o seu registo. | n/a | |
2. O sistema solicita os dados do Utilizador (credenciais de acesso, nome, e-mail). | Quem cria Utilizador? | RegistoUtilizadores |
| | E quem mantém RegistoUtilizadores? | CentroExposição |
3. O Utilizador (não registado) introduz os dados solicitados. | Onde são guardados os dados solicitados? | Utilizador |
| | Quem vai fazer a introdução dos dados? | CentroExposição | |
4. O sistema valida e solicita que o Utilizador (não registado) confirme os dados inseridos. | Quem valida os dados? | RegistoUtilizadores |
5. O Utilizador (não registado) confirma os dados. | n/a |  |
6. O sistema regista os dados e informa o Utilizador do sucesso da operação. | Quem guarda Utilizador? | RegistoUtilizadores |

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* CentroExposições 
* Utilizador
* RegistoUtilizadores  

Outras classes de software (i.e. Pure Fabrication) identificadas:  

* RegistarUtilizadorUI
* RegistarUtilizadorController


## Diagrama de Sequência ##
![SD UC 6 - IT 1.png](UC6-DS.jpg)

## Diagrama de Classes ##
![DC - UC 6.png](UC6-DC.jpg)