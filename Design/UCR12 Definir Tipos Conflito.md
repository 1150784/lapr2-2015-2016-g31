# Realiza��o do UC12 Definir Tipo de Conflito de Interesse #

## Racional ##

Fluxo Principal | Quest�o: Que Classe... | Resposta | Justifica��o
--------------- | ---------------------- | -------- | ------------
1. O gestor de exposi��es inicia a defeni��o dos tipos de conflito de interesse. | Que classe controla a de3feni��o dos tipos de conflitos de interesse. |DefinirConflitoController  | Controller
2. O sistema apresenta todas exposi��es. | Que classe tem registo das exposicoes? | RegistoExposicoes  | A classe RegistoExposicoes tem registo de todas as exposicoes.
3. O gestor de exposi��es indica a exposi��o que pretende usar.  |  | 
4. O sistema pede a introdu��o do tipo de conflito.  | 	Que classe guarda estes dados? | |
5. O gestor de exposi��es insere o tipo de conflito.  | quem guarda as altera��es introduzidas?|RegistoCandidatura |
6. O sistema valida os dados introduzidos e pede confirma��o. | | | 
7. O Gestor confirma.| Que classe valida estes dados?| Conflito |valida��o local
|||Registo conflitos|Valida��o global
8. O sistema regista o conflito de interesse.|que classe regista os dados?|Registo conflitos|� ondle os conflitos est�o guardados
9. Repete os passos 4 a 7 ate o gestor decidir continuar.|||
10. Repete os passos 3 a 7 ate o gestor decidir parar.|||

## Sistematiza��o: ##
Do racional resulta que as classes conceptuais promovidas a classes de software s�o:

* Centro de Exposi��es
* Conflito
* RegistoConflito

Outras classes de software (i.e. Pure Fabrication) identificadas:

* DefinirConflitoUI
* DefinirConflitoController
 

## Diagrama de Sequ�ncia ##

![UC12-DS](UC12-DS.jpg)

## Diagrama de Classes ##

![UC12-DC](UC12-DC.jpg)
