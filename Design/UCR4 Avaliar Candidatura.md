﻿# Realização do UC4 Avaliar Candidatura#

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O FAE inicia no sistema o processo de avaliação sobre candidaturas. | Guarda as Atribuições | Exposição | porque exposição tem instancias de atribuição.
2. O sistema mostra uma lista de exposições em que o utilizador FAE é um dos avaliadores. | Guarda as exposições | RegistoExposições | guarda todos os objetos Exposição.
3. O FAE seleciona uma exposição. | | |
4. O sistema mostra uma lista de candidaturas que requerem uma avaliação do FAE. | Guarda as candidaturas | RegistoCandidaturas | porque guarda todos os objetos candidaturas.
5. O FAE escolhe uma candidatura. | | |
6. O sistema mostra a informação que caracteriza a candidatura e solicita a indicação se a candidatura deve ser aceite ou recusada. | | |
7. O FAE indica no sistema se a candidatura é aceite ou recusada e introduz uma breve texto justificativo. | guarda a avaliação | Avaliação | tem a informação de aceitação.
8. O sistema valida a avaliação e pede ao FAE confirme a avaliação como definitiva. | valida a avaliação | exposição | tem instancias de avaliação.
9. O FAE confirma. | | |
10. O sistema regista a decisão tomada. | regista a decisão | Candidatura| Tem instancias de avaliação.
11. Os passos 4 a 10 repetem-se até que todas as candidaturas tenham sido tratadas. | | |
12. O sistema informa o utilizador que as avaliações ficaram registadas e o caso de uso termina. | | |

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* 


Outras classes de software (i.e. Pure Fabrication) identificadas:  

* 


## Diagrama de Sequência ##
![SD UC 4 - IT 1.jpg](UC4-DS.jpg)

## Diagrama de Classes ##
![DC UC 4 - IT 1.jpg](UC4-DC.jpg)IT 1.jpg](UC4-DC.jpg)