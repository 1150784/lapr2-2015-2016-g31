# Realização do UC13 Detetar Conflitos #

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* ListaConflitos
* Conflito
* MecanismoDetecaoConflito
* ListaTipoConflito
* TipoConflito

Outras classes de software (i.e. Pure Fabrication) identificadas:

* DetecaoConflitosController
 

## Diagrama de Sequência ##

![UC13-DS](UC13-DS1.jpg)
![UC13-DS2](UC13-DS2.jpg)

## Diagrama de Classes ##

![UC13-DC](UC13-DC.jpg)
