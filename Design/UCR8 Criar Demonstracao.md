﻿# Realização do UC8 Criar Demonstração #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O organizador inicia a criação de uma demonstração.| guarda as demonstrações| Exposição |
2. O sistema lista as exposições do organizador.|guarda as exposições |RegistoExposições |
3. O organizador seleciona uma exposição.| | |
4. O sistema lista os recursos disponiveis e solicita a escolha dos recursos necessarios.|onde estão guardados os recursos |RegistoRecursos |
5. O organizador escolhe os recursos que deseja.| | |
6. O sistema pede confirmação.| | |
7. O organizador confirma.| | |
8. Os passos 4 a 6 repetem-se ate que todos os recursos necessarios sejam selecionados.| | |
9. O sistema pede que insira os dados da demonstração.| | |
10. O organizador insere os dados da demonstração.| | |
11. O sistema pede confirmação.| | |
12. O organizador confirma.| | |
13. O sistema regista as informações e informa do sucesso da operação.|onde fica registada a informação |ListaDemonstrações |

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* 

Outras classes de software (i.e. Pure Fabrication) identificadas:

*
 

## Diagrama de Sequência ##

![UC8-DS](UC8-DS.jpg)

## Diagrama de Classes ##

![UC8-DC](UC8-DC.jpg)
