﻿# Realização do UCR16 Taxa de aceitação #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Gestor/Organizador de Exposições inicia taxa de aceitação. | Que classe controla a taxa de aceitação? | TaxaAceitacaoController | É o controller
2.  O sistema lista todas as exposições e pede para escolher uma.|Que classe guarda todas as exposições?|RegistoExposicao|
3. O Gestor/Organizador escolhe uma exposição.||| 
4. O sistema mostra a taxa de aceitação de expositores e os valores médios de cada critério de submissão. |||
5. O Caso de uso termina.| | |

## Sistematização: ##
Do racional resulta que as classes conceptuais promovidas a classes de software são:



Outras classes de software (i.e. Pure Fabrication) identificadas:

* TaxaAceitacaoUI
* TaxaAceitacaoController


## Diagrama de Sequência ##



## Diagrama de Classes ##



