﻿# Realização do UCR2 Decidir demonstrações #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Organizador de exposições inicia no sistema o processo de decisão de demonstrações.| Que classe controla a decisão de demonstração? | DecisaoDemonstracaoController | É o controller
2.  O sistema lista todas as demonstrações.|Que classe guarda todas as exposições?|ListaDemonstracoes|Lista demonstrações contém todas as demonstrações
3. O Organizador escolhe as demonstrações que funcionarão.| muda o estado da demonstração? | DecisaoDemonstracao | IE
4. O sistema pede para definir um período de tempo. |||
5. O organizador define um período de candidatura às demonstrações.| | |
6. O sistema pede confirmação.|||
7. O organizador confirma.|||
8. O sistema informa do sucesso.|||

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* DecisaoDemonstracao


Outras classes de software (i.e. Pure Fabrication) identificadas:

* DecisaoDemonstracaoUI
* DecisaoDemonstracaoController


## Diagrama de Sequência ##



## Diagrama de Classes ##




