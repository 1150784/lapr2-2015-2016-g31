
# Realização do UC2 Definir FAE #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O organizador inicia no sistema a definição dos FAE. |n/a||
2. O sistema mostra a lista das exposições sem FaeDefinidos, nas quais utilizador é organizador. |  ... conhece todas as exposições do organizador ? |  RegistoExsposições | IE: um Registoexposições tem várias exposições.
| ...conhece o registoExposições? | Centro Exposições | IE: no DC o centro de exposições tem um RegistoExposições.
3. O organizador seleciona a exposição pretendida. |n/a	||
4. O sistema solicita a identificação dum utilizador para membro da FAE. | n/a ||
5. O organizador introduz o identificador do novo membro. | .. cria o objeto FAE?|	ListaFAE|	Creator: Regra1(HC +LC)
| .. conhece a ListaFAE | Exposição | IE: no DC a Exposição tem uma LISTAFAE
|..associa o objeto Utilizador ao objeto FAE?|	ListaFAE|	IE: uma ListaFAE tem vários FAE.
6. O sistema valida e solicita confirmação.| ...valida os dados do FAE?|	FAE|	IE: Validação local.
		||ListaFAE|	IE: Validação global.
7. O organizador confirma os dados.|	n/a	||
8. O sistema regista o novo membros.|  ...guarda os dados do FAE?|	ListaFAE|	IE: uma ListaFAE tem vários FAE.  
9. Os passos 4-8 são repetidos até a lista de FAE estar completa.
10. O Sistema altera a condição da exposição e informa o organizador o sucesso da operação. |.... cria o estado completa ou cria o estado FaeSemDemonstração ? | ExposiçãoCompletaState/ ExposiçãoFaeSEmDemonstraçãoState | Padrão State.
|... regista o estado completa/FAESemDemonstração? | Exposição  | Ie: A exposição tem um estado associado.		


## Diagrama de Sequencia ##

![UC2-DefinirFAE.jpg](UC2-DS.jpg)


## SetFAEDefinidos ##

![EstadoDaExposição.jpg](UC2-DS2.jpg)


## Diagrama de Classes

![UC2-DC.jpg](UC2-DC.jpg)