# Realização do UC19 Retirar Candidatura #

## Racional ##

**Que classe coordena o UC?**  
Pelo GRASP podemos usar o padrão Controller: implica a existência de uma classe “RegistarCandidaturaController”.   
Esta classe é responsável por coordenar/distribuir as ações realizadas na User Interface (UI) com o resto do sistema.

**Regras de Creator:** 
> Problema: Que classe é responsável por criar objetos de uma outra classe?  
> Solução: Define-se de acordo com as regras GRASP:  
> > 1. B contém ou agrega objetos da classe A (preferencial)  
> > 2. B regista instâncias da classe A  
> > 3. B possui os dados usados para inicializar A  
> > 4. B está diretamente relacionado com A  
> > Se mais de uma condição se aplica, escolhe-se a classe “B que contém ou agrega objetos da classe A”

**Outras Responsabilidades**


Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O representante do expositor inicia no sistema o processo de retirar candidaturas. | n/a | |
2. O sistema lista todas as candidaturas que estejam no estado "em avaliação". | ... tem a lista de candidaturas que estão no estado registada? |ListaCandidaturas |Creator: regra 1.
3.  O representante do expositor seleciona uma exposição. |n/a | |
4. O sistema mostra as informações relativas à candidatura e pede confirmação. |...tem as informações? |ListaCandidaturas |IE: a Candidatura tem as informações.
5. O representante confirma . |n/a | | 
6. O sistema solicita se pretende continuar ou retirar mais candidaturas. |n/a | | 
7. O representante seleciona a opção. |...muda o estado da Candidatura ?|RetirarCandidatura | IE
8. Os passos 2 a 7 repetem-se enquanto o representante pretender retirar mais candidaturas. |n/a | |
9. O sistema informa o sucesso da operação. |n/a | |


## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* Centro de Exposições  
* Candidatura
* ListaCandidaturas


Outras classes de software (i.e. Pure Fabrication) identificadas:  

* RetirarCandidaturaUI
* RetirarCandidaturaController


## Diagrama de Sequência ##


## Diagrama de Classes ##

Outras classes de software (i.e. Pure Fabrication) identificadas:  

* RegistarCandidaturaUI
* RegistarCandidaturaController


## Diagrama de Sequência ##


## Diagrama de Classes ##


