# Realização do UC10 Alterar Perfil do Utilizador

## Racional

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Utilizador solicita a alteração do seu perfil. | Quem mantém o registo de Utilizadores? | RegistoUtilizadores  | |
| | Como temos acesso ao RegistoUtilizadores? | RegistoUtilizadoresController | |
2. O sistema abre uma interface e apresenta os dados correntes do Utilizador. | Onde estão os dados do Utilizador?  | Utilizador |
3. O Utilizador selecciona (apenas uma escolha) a informação que pretende alterar. | n/a | |
4. O sistema apresenta uma janela em que solicita a nova informação. | n/a |  |
5. O Utilizador faz a alteração à informação que seleccionou. | Onde será armazenada (temporariamente) esta nova informação? | RegistoUtilizadoresController |
6. O sistema compara a informação anterior e a nova informação e solicita uma confirmação. | n/a | |
7. O Utilizador confirma a alteração. | Onde vão estar os dados depois desta confirmação? | RegistoUtilizadoresController | Os novos dados apenas serão enviados para Utilizador após o utilizador guardar todas as alterações que fez para evitar que seja chamado um método de setDados se ainda não foi terminada a operação por completo.
8. O sistema regista os novos dados e informa o Utilizador do sucesso da operação. | n/a | |
9. Repetir passo 3 até o Utilizador fazer todas as alterações que achar necessário. | n/a | |
10. O Utilizador pede para guardar as alterações feitas ao seu perfil. | n/a | |
11. O sistema guarda as alterações do Utilizador e termina a alteração do perfil. | Onde vão ser guardadas as alterações? | Utilizador | É neste último ponto que vamos então enviar, de uma só vez, para Utilizador todos os dados que foram alterados.


## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* Utilizador
* RegistoUtilizadores

Outras classes de software (i.e. Pure Fabrication) identificadas:

* AlterarPerfilUtilizadorUI
* AlterarPerfilUtilizadorController
 

## Diagrama de Sequência ##

![UC10-DS](UC10-DS.jpg)

## Diagrama de Classes ##

![UC10-DC](UC10-DC.jpg)
