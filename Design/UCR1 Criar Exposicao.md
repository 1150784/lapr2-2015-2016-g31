# Realização do UC1 Criar Exposição #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Gestor de Exposições inicia o caso de uso. | Onde vão estar guardadas todas as Exposições criadas? | RegistoExposições | Classe que vai manter todos os registos das exposições
2. O sistema solicita os dados necessários para a criação de uma Exposição. | Quem cria Exposição? | RegistoExposições |
3. O Gestor de Exposições introduz os dados solicitados. | Para onde são enviados os dados? | Exposição |
4. O sistema valida e mostra uma lista de utilizadores (para escolher os Organizadores). | Quem valida? | Exposição |
| | Onde está guardada a lista de utilizadores? | RegistoUtilizadores | Classe que vai manter todos os registos dos utilizadores.
5. O Gestor de Exposições selecciona um utilizador. | n/a | |
6. O sistema apresenta os dados do utilizador. | Quem tem os dados do utilizador? | Utilizador |
7. O Gestor de Exposições define-o como Organizador. | Quem vai criar Organizador? | Exposição |
| | Quem adiciona Organizador à Exposição? | Exposição |
| | Quem valida Organizador? | Exposição |
| | Onde são guardados todos os dados relevantes a Organizador? | Utilizador | 
8. Repetir passos 4-7 e repetir as vezes que o Gestor de Exposições desejar. | n/a | | |
9. O sistema valida os Organizadores e solicita uma confirmação. | Quem valida? | Exposição |
| | Onde são guardados os Organizadores? | Exposição |
10. O Gestor de Exposições confirma todos os dados. | n/a | |
11. O sistema cria a exposição e avisa o Gestor de Exposições do sucesso da operação. |  | |


## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* Centro de Exposições
* Exposição
* Organizador
* Local

Outras classes de software (i.e. Pure Fabrication) identificadas:

* CriarExposiçãoUI
* CriarExposiçãoController
* RegistoExposicoes
* RegistoUtilizadores
* ListaOrganizadores
* ExposicaoEstado
* ExposicaoCriadaEstado
* ExposicaoFAESemDemoEstado
* ExposicaoDemoSemFAEEstado
* ExposicaoCompletaEstado


## Diagrama de Sequência ##

![UC1-DS](UC1-DS.jpg)
![UC1-DS](UC1-DS2.jpg)
![UC1-DS](UC1-DS3.jpg)

## Diagrama de Classes ##

![UC1-DC](UC1-DC.jpg)
