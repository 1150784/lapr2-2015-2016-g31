﻿
# Realização do UC3 Atribuir Candidatura a FAE#

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O organizador inicia no sistema a atribuição de candidaturas. | ... cria a associação de candidaturas a FAE? | Atribuição |  Guarda informaçãoedo FAE e da Candidatura.
2. O sistema mostra as exposições ativas para o organizador e solicita escolha de uma. | ... conhece todas as exposições? | Centro de Exposições, Exposição | IE: Centro de Exposições agrega instâncias de Exposição.
3. O organizador seleciona uma exposição. | | |
4. O sistema mostra candidaturas e mecanismos da exposição e solicita o mecanismo de emparelhamento entre candidaturas e FAEs. | ... conhece todas as candidaturas das exposições? | RegistoCandidatura |  RegistoCandidatura guarda todos os objetos de Candidatura .
|Conhece os mecanismos de atribuição? | RegistoMecanismos | guarda todos os objetos de mecanismo. 
5. O organizador seleciona um mecanismo. | | |
6. O sistema expõe as assosiações criadas pelo mecanismo selecionado. | ... conhece todas as associações entre candidaturas e FAE, para poder validar o critério de paragem? | Atribuição| Guarda todas as associações.
7. O sistema valida e pede confirmação. | ... valida as associações entre candidaturas e FAEs? | Exposição, Decisão | IE: Exposição agrega instâncias de Decisão.
8. Os passos 5 a 8 repetem-se ate que o organizador confirme as associações.| | |
9. O sistema regista a atribuição e informa o sucesso da operação. | ... regista as associações entre candidaturas e FAE? | Exposição | IE: Exposição agrega instâncias de Atribuição.


## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* FAE
* Organizador
* Atribuição
* Candidatura


Outras classes de software (i.e. Pure Fabrication) identificadas:  

* AtribuirCandidaturaUI
* AtribuirCandidaturaController
* <<Interface>> MecanismoAtribuição


## Diagrama de Sequência ##
![SD UC 3 - IT 1.jpg](UC3-DS.jpg)
![SD UC 3 - IT 1.jpg](UC3-DS2.jpg)

## Diagrama de Classes ##
![DC UC 3 - IT 1.jpg](UC3-DC.jpg)