# UCR14 Alterar Conflitos de Interesse #

## Racional ##


Fluxo Principal 	| Questão: Que Classe...    | Resposta     | Justificação
----------------------- | ------------------------- | ------------ | -------------
1. O FAE inicia a alteração de conflitos de interesse. | n/a |  |		
2. O sistema mostra a lista de Exposições que o FAE pertence. | Que classe contém Exposicao? | RegistoExposicoes | O RegistoExposicoes tem objetos da classe Exposicao.
| | Que classe contém RegistoExposicoes? | CentroExposicoes | O CentroExposicoes tem RegistoExposicoes.
3. O FAE escolhe a exposição. | n/a |  |		
4. O sistema mostra a lista de candidaturas a que o FAE foi designado a avaliar. | Que classe tem o registo das Candidaturas por avaliar? | Exposicao | Exposicao tem registo de Candidaturas.
5. O FAE escolhe a candidatura. | n/a |  |		
6. O sistema mostra uma lista de conflitos de interesse. | Que classe vai conter um registo de todos os conflitos? | RegistoConflitos | O RegistoConflitos tem objetos da classe Conflito.
  || Que classe conhece a classe RegistoConflitos? | CentroExposicoes | O CentroExposicoes tem RegistoConflitos
7. O FAE selecciona o conflito de interesse. | n/a |  |	
8. O sistema mostra os dados do conflito de interesse. | Que classe vai ter a informação do Conflito? | TipoConflito | A classe TipoConflito contém os atributos de Conflito.
9. O FAE altera/remove o conflito. | n/a |  |		
10. O sistema valida e pede confirmação. | Que classe vai validar a alteração do conflito? | Conflito | A classe Conflito contém os seus atributos.
| | Que classe elimina o conflito? | RegistoConflitos | O RegistoConflitos contém Conflitos.
11. O FAE confirma. | n/a |  |	
12. Repetir desde o passo 6 até o utilizador estar satisfeito.	
13. O sistema regista a informação e informa do sucesso da operação. | Quem regista as alterações? | RegistoConflitos |

### Sistematização: ###
Do racional resulta que as classes concetuais promovidas a classes de software são:

* Centro de Exposições
* Conflito
* Exposicao

Outras classes de software (i.e. Pure Fabrication) identificadas:

* AlterarConflitoInteresseUI
* AlterarConflitoInteresseController
* RegistoConflitos
* RegistoExposicoes

## Diagrama de Sequência ##
![UC14_DS.jpg](UC14-DS.jpg)

* sd getListaExposicoesFAE
![UC14-DS2.jpg](UC14-DS2.jpg)

![UC14-DS3.jpg](UC14-DS3.jpg)

## Diagrama de Classes ##
![UC14_DC.jpg](UC14-DC.jpg)