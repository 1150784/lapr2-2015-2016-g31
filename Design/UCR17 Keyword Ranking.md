﻿# Realização do UCR17 Keyword Ranking #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Gestor de Exposições inicia o Ranking das exposições | Que classe controla o ranking? |KeywordRankingController | É o controller
2. O sistema apresenta o rank das keywords em forma de lista e pede confirmação.|Que classe guarda as keywords?|ListaKeyword|
3. O Gestor confirma||| 
4. O sistema cria um ficheiro csv e termina o caso de uso.|||


## Sistematização: ##
Do racional resulta que as classes conceptuais promovidas a classes de software são:



Outras classes de software (i.e. Pure Fabrication) identificadas:
* KeywordRankingUI
* KeywordRankingController 
* ListaKeyword

## Diagrama de Sequência ##

![UC17-DS](UC17-DS.jpg)

## Diagrama de Classes ##

![UC17-DC](UC17-DC.jpg)
