# Realização do UCR15 Criar Stand #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Gestor de Exposições inicia a criação de um stand. | ... cria a classe Stand? | RegistoStands em Centro de Exposições. | Creator (Regra 1) + High Cohesion.
2.  O Sistema solicita os dados (descrição) do Stand. | n/a ||
3.  O Gestor de Exposições introduz os dados do stand. | ... guarda os dados do Stand? | Stand | 	IE: A classe Stand contém os seus atributos.
4. O Sistema valida e pede confirmação dos dados do stand. | ... valida os dados do Stand? | Stand | IE: O próprio Stand deve ser capaz de fazer a validação dos seus dados (validação local)
 ||| RegistoStands | IE:O RegistoStands contém os stands (validação global)
5. O Gestor de Exposições confirma os dados do Stand. | n/a ||
6. Os passos 2 a 5 são repetidos até que todos os stands estejam criados. | n/a ||
7. O sistema regista os dados e informa o Gestor de exposições do sucesso da operação. | ... regista os dados inseridos? | RegistoStands | IE: RegistoStands contém stands.

## Sistematização: ##
Do racional resulta que as classes conceptuais promovidas a classes de software são:

* Centro de Exposições
* Stand

Outras classes de software (i.e. Pure Fabrication) identificadas:

* CriarStandUI
* CriarStandController
* RegistoStands (High Cohesion)



## Diagrama de Sequência ##

![UC15-DS](UC15-DS.jpg)

## Diagrama de Classes ##

![UC15-DC](UC15-DC.jpg)
