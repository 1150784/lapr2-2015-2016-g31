# Realização do UCR23 Confirmar Stand #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O representante do expositor inicia no sistema o processo de confirmar o stand atribuído a uma candidatura | ... cria a classe Stand? | RegistoStands em Centro de Exposições. | Creator (Regra 1) + High Cohesion.
2. O sistema mostra a lista de candidaturas a que o representante está ligado e solicita a escolha de uma.|...lista candidaturas? |Centro de Exposições |IE
3. O representante seleciona uma candidatura. | n/a | |
4. O sistema mostra o stand atribuído á candidatura e sua descrição e pede a escolha de um decisão. |... tem o stand atribuído? |Candidatura |IE
5.  O representante seleciona uma opção. | n/a| | 
6. O sistema pede confirmação. | | |
7. O representante confirma. | | |
8. Os passos 2 a 8 repetem-se enquanto o representante pretender retirar mais candidaturas. | | | 
9. O sistema informa o sucesso da operação. |... informa? |Exposição | IE


## Sistematização: ##
Do racional resulta que as classes conceptuais promovidas a classes de software são:

* Centro de Exposições
* Candidatura
* Stand

Outras classes de software (i.e. Pure Fabrication) identificadas:

* ConfirmarStandUi
* ConfirmarStandController

## Diagrama de Sequência ##



## Diagrama de Classes ##

