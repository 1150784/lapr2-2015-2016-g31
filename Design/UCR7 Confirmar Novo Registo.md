﻿# Realização do UC7 Confirmar Novo Registo #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Gestor de Exposições inicia o sistema de confirmação de novos utilizadores. | n/a | |
2. O sistema apresenta uma lista de utilizadores por validar. | Onde é criada a lista? | CentroExposições | É onde se encontram todos os utilizadores.
3. O Gestor de Exposições selecciona um  utilizador.  | Onde estão guardados os utilizadores? | CentroExposições |
4. O sistema devolve os dados do utilizador seleccionado. | De onde vêm os dados de cada utilizador? | Utilizador | 
5. O Gestor de Exposições verifica os dados do novo registo e confirma. | Para onde é enviada a confirmação? | CentroExposições |
6. O sistema verifica a existência de duplicados e guarda os dados. | n/a | | 

## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* CentroExposições 
* Utilizador  

Outras classes de software (i.e. Pure Fabrication) identificadas:  

* ConfirmarNovoRegistoUI
* ConfirmarNovoRegistoController


## Diagrama de Sequência ##
![SD UC 7 - IT 1.jpg](UC7-DS.jpg)

## Diagrama de Classes ##
![DC - UC 7.jpg](UC7-DC.jpg)