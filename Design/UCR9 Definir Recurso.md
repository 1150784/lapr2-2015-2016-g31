# Realização do UC9 Definir Recurso #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Gestor de Exposições solicita a definição de recursos. | Quem vai apresentar todos os recursos? | RegistoRecursos |
2. O sistema apresenta todos os dados já existentes na base de dados. | Quem mantém o controlo de RegistoRecursos? | CentroExposição |
3. O Gestor de Exposições inicia a criação de um novo recurso. | Quem cria Recurso? | RegistoRecursos  |
4. O sistema solicita os dados. | n/a | |
5. O Gestor de Exposições insere todos os dados necessários acerca do recurso a ser utilizado. | n/a | |
6. O sistema valida os dados e solicita confirmação. | Quem faz a validação dos dados? | Recurso |
7. O Gestor de Exposições confirma. | Quem faz a validação de Recurso? | RegistoRecursos |
8. O sistema guarda os dados. | Quem vai guardar os dados? | Recurso |
9. São repetidos os passos 3-7 até o Gestor de Exposições estar satisfeito com a operação. | n/a | |
10. O sistema informa o Gestor de Exposições sobre o sucesso da operação. | n/a | |


## Sistematização: ##

Do racional resulta que as classes conceptuais promovidas a classes de software são:

* CentroExposição
* Recurso
* RegistoRecursos

Outras classes de software (i.e. Pure Fabrication) identificadas:

* DefinirRecursoUI
* DefinirRecursoController
 

## Diagrama de Sequência ##

![UC9-DS](UC9-DS.jpg)

## Diagrama de Classes ##

![UC9-DC](UC9-DC.jpg)
