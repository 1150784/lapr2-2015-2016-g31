﻿# Realização do UCR21 Atribuir Stands #

## Racional ##

Fluxo Principal | Questão: Que Classe... | Resposta | Justificação
--------------- | ---------------------- | -------- | ------------
1. O Organizador de Exposições inicia o processo de atribuibuição de stands.|...guarda a atribuição dos stands?| RegistoAtribuiçãoStand| 
2. O sistema apresenta uma lista das candidaturas com decisão.| ...guarda as candidaturas?|RegistoCandidaturas| Tem todas as instancias de candidaturas
3. O Organizador seleciona uma.||| 
4. O sistema apresenta a informação da candidatura, apresenta uma lista dos stands e solicita a escolha de um.| ... guarda os stands| RegistoStands|
5. O Organizador seleciona o stand.|||
6. O sistema pergunta se quer continuar|||
7. O Organizador responde afirmativamente.|||
8. O sistema pede confirmação.|||
9. O Organizador confirma.|||
10. O sistema regista a atribuição e termina o processo.|||

## Sistematização: ##
Do racional resulta que as classes conceptuais promovidas a classes de software são:



Outras classes de software (i.e. Pure Fabrication) identificadas:



## Diagrama de Sequência ##

![UC21-DS](UC21-DS.jpg)

## Diagrama de Classes ##

![UC21-DC](UC21-DC.jpg)
